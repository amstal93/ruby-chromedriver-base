# ruby-chromdriver-base

Docker image, which could (and should) be used as base image for other images. It brings and ships Ruby, Chrome and Chromehelper, so it is perfect e.g. was test ENVs with Selenium etc.

## Current tool versions:

* Ruby: `2.5.0`
* Chrome: Always latest and stable version for Linux
* Chromedriver: `2.39`

## Usable `ENV` vars:

* `CHROMEDRIVER_VERSION`: Points to current used Chromedriver version
* `CHROMEDRIVER_DIR`: `/usr/bin`

## Usage:

```Dockerfile
FROM registry.gitlab.com/vehiculum/ruby-chromedriver-base:latest

# YOUR OWN parts come here
```